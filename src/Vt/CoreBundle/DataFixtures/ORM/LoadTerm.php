<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Vt\CoreBundle\Entity\Term;
use Vt\CoreBundle\Entity\Translation;

class LoadTerm implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $terms = array(
            array(
                'term' => 'Hello',
                'translations' => array(
                    array(
                        'type' => 'verb',
                        'translation' => 'hola',
                        'example' => 'Hola amigo',
                        'active' => true,
                    ),
                    array(
                        'type' => 'sustantive',
                        'translation' => 'hola hola',
                        'example' => 'Hola amigo mio',
                        'active' => true,
                    ),
                ),
            ),
            array(
                'term' => 'Friend',
                'translations' => array(
                    array(
                        'type' => 'sustantive',
                        'translation' => 'amigo',
                        'example' => 'Que pasa amigo',
                        'active' => true,
                    ),
                ),
            ),
        );

        foreach ($terms as $term) {
          $termInstance = new Term();

          $termInstance->setName($term['term']);

          foreach ($term['translations'] as $translation) {
            $translationInstance = new Translation();
            $translationInstance->setType($translation['type']);
            $translationInstance->setTranslation($translation['translation']);
            $translationInstance->setExample($translation['example']);
            $translationInstance->setIsActivated($translation['active']);
            $translationInstance->setTerm($termInstance);

            $manager->persist($translationInstance);
          }

          $manager->persist($termInstance);
        }

        $manager->flush();
    }
}
