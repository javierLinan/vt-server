<?php

namespace Vt\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Vt\CoreBundle\Entity\Term;
use Vt\CoreBundle\Entity\Translation;
use Vt\CoreBundle\Form\TermType;
use Vt\CoreBundle\Form\TranslationType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Doctrine\ORM\NoResultException;

class DefaultController extends FOSRestController
{

    public function getTermsAction()
    {

        $repository = $this->getDoctrine()->getRepository('VtCoreBundle:Term');
        $terms = $repository->findAll();

        $view = $this->view(array('terms' => $terms), 200);

        return $this->handleView($view);

    }

    public function postTermsAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $term = new Term();
        $form = $this->createForm(TermType::class, $term);
        $form->submit($data);

        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($term);
            $em->flush();

            $view = $this->view(array('term' => $term), 201);
            return $this->handleView($view);
        }

        $view = $this->view($form, 400);
        return $this->handleView($view);
    }

    public function getTermAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('VtCoreBundle:Term');
        $term = $repository->find($id);
        $view = $this->view(array('term' => $term), 200);
        return $this->handleView($view);
    }

    public function getTermTranslationsAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('VtCoreBundle:Term');
        $term = $repository->find($id);
        $translations = $term->getTranslations();
        $view = $this->view(array('translations' => $translations), 200);
        return $this->handleView($view);
    }

    public function postTermTranslationsAction(Request $request, $id)
    {
        $data = json_decode($request->getContent(), true);

        $translation = new Translation();
        $form = $this->createForm(TranslationType::class, $translation);
        $form->submit($data);

        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine()->getRepository('VtCoreBundle:Term');

            $term = $repository->find($id);
            $translation->setTerm($term);

            $em->persist($translation);
            $em->flush();

            $view = $this->view(array('translation' => $translation), 201);
            return $this->handleView($view);
        }

        $view = $this->view($form, 400);
        return $this->handleView($view);
    }

    public function putTranslationsAction(Request $request, $id)
    {
        $data = json_decode($request->getContent(), true);


        $translation = $this->getDoctrine()->getRepository('VtCoreBundle:Translation')->find($id);

        $form = $this->createForm(TranslationType::class, $translation);
        $form->submit($data);

        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($translation);
            $em->flush();

            $view = $this->view(array('translation' => $translation), 200);
            return $this->handleView($view);
        }

        $view = $this->view($form, 400);
        return $this->handleView($view);
    }

    public function deleteTranslationAction($id) {
        $translation = $this->getDoctrine()->getRepository('VtCoreBundle:Translation')->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($translation);
        $em->flush();

        $view = $this->view(array('translation_id' => $id), 200);
        return $this->handleView($view);
    }

    public function postTermTranslationAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $term = new Term();
        $form = $this->createForm(TermType::class, $term);
        $form->submit(array('name' => $data['term']));

        if($form->isValid()) {
            unset($data['term']);

            $translation = new Translation();
            $form = $this->createForm(TranslationType::class, $translation);
            $form->submit($data);


            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $translation->setTerm($term);

                $em->persist($term);
                $em->persist($translation);
                $em->flush();

                $view = $this->view(array('term' => $term, 'translation' => $translation), 201);
                return $this->handleView($view);
            }
        }

        $view = $this->view($form, 400);
        return $this->handleView($view);
    }
}
