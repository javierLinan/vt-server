# VOCABULARY TRAINER - SERVER # 

## Git workflow ##

- Ticket definition in the README file Changelog section.
- We create a branch for the ticket.
```
git checkout -b tXXXX
```
- We work on the new branch.
```
git checkout tXXXX
...
git commit -m "[Commit type] Brief explanation #XXXX"
...
```
- The new branch is merged with master without fast forward.
```
git checkout master
git merge --no-ff tXXXX
```

## Commit labels ##
- STYLE: Cosmetic changes (change file names, sort folders, etc).
- README: README file updated (Tickets added, new commit types, instructions, etc).
- FIX: Fixing an error or a wrong functionality.

## Changelog ##
- **t001**: New method in the API to CREATE a TERM and a TRANSLATION from the data received.
